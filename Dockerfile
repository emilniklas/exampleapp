FROM loalang/loa AS builder

RUN mkdir /ExampleApp
WORKDIR /ExampleApp

COPY .pkg.lock .

RUN loa pkg get --no-update

COPY . .

RUN loa build -o /ExampleApp.loabin

FROM loalang/vm

COPY --from=builder /ExampleApp.loabin /ExampleApp.loabin

CMD ["/ExampleApp.loabin"]